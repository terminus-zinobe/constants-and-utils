# HTTP messages
OK = 'ok'
CREATED = 'created'
ACCEPTED = 'accepted'
NOT_CONTENT = 'not_content'
BAD_REQUEST = 'bad_request'
UNAUTHORIZED = 'unauthorized'
PAYMENT_REQUIRED = 'payment_required'
FORBIDDEN = 'forbidden'
METHOD_NOT_ALLOWED = 'method_not_allowed'
NOT_ACCEPTABLE = 'not_acceptable'
NOT_FOUND = 'not_found'
INTERNAL_SERVER_ERROR = 'internal_server_error'
BAD_GATEWAY = 'bad_gateway'
GATEWAY_TIMEOUT = 'gateway_timeout'

# Actions
SAVED = 'saved'

# Messages
BAD_CREDENTIALS = 'bad_credentials'
SIGNATURE_EXPIRED = 'signature_expired'
INVALID_TOKEN = 'invalid_token'
ERROR = 'error'
EXISTING_RESOURCE = 'existing_resource'
ID_NOT_VALID = 'id_not_valid'
CURRENCY_NOT_FOUND = 'currency_not_found'
